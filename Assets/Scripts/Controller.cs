﻿using UnityEngine;
using System.Collections;
using System;

public class Controller : MonoBehaviour {

	public float moveSpeed = 6f;

    public float minimumMouseDst = 0.1f;

    Rigidbody2D rb;
	Vector2 velocity;

	void Start () 
	{
		rb = GetComponent<Rigidbody2D> ();
	}

	void Update () 
	{
		Vector3 diff = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        
        if (Mathf.Abs(diff.x) >= minimumMouseDst || Mathf.Abs(diff.y) >= minimumMouseDst)
        {
            diff.Normalize();
            float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(0f, 0f, rot_z);
        }

		velocity = new Vector2 (Input.GetAxisRaw ("Horizontal"), Input.GetAxisRaw ("Vertical")).normalized * moveSpeed;
	}

	void FixedUpdate() 
	{
        rb.MovePosition(rb.position + velocity * Time.fixedDeltaTime);
        Camera.main.transform.position = new Vector3(rb.position.x, rb.position.y, Camera.main.transform.position.z);
	}
}
