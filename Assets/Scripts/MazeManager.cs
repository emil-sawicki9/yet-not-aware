﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class MazeManager
{
    public int width = 10;
    public int height = 10;

    private Cell[,] _maze;
    private System.Random _rand;

    private enum Direction
    {
        N = 0, E, S, W
    }

    public MazeManager(int w, int h)
    {
        _rand = new System.Random();
        width = w;
        height = h;
    }

    private void reset()
    {
        _maze = new Cell[width, height];
    }

    public void generateMaze()
    {
        reset();

        Stack<Cell> stack = new Stack<Cell>();

        Cell currCell = new Cell(_rand.Next(width), _rand.Next(height));
        _maze[currCell.getX(), currCell.getY()] = currCell;
        stack.Push(currCell);
        int _debugMax = width*height*3;
        int _debugCurr = 0;
        while(stack.Count > 0 && _debugCurr < _debugMax)
        {
//            if (_debugMax % 10 == 0)
//                _rand = new System.Random();
            currCell = getNotVisitedCell(currCell.getX(), currCell.getY(), ref currCell);
            if (currCell == null)
                currCell = stack.Pop();
            else
                stack.Push(currCell);
            _debugCurr++;
        }
    }

	public void drawMaze(GameObject wallPrefab, GameObject wallContainer, bool drawPathways = false)
    {
        float offset = 1.5f;
        float width = 3f;
        for (int i = 0; i < _maze.GetLength(0); i++)
        {
            for (int j = 0; j < _maze.GetLength(1); j++)
            {
                // 0xNESW
                Cell c = _maze[i, j];
                if ((c.walls & 0x1000) == 0x1000)
                {
                    GameObject.Instantiate(wallPrefab, new Vector3(i * width + offset, -j * width, 0), Quaternion.Euler(0, 0, 0), wallContainer.transform).name = "wall [" + i + "," + j + "] " + c.walls.ToString("X4") + " [N]";
                }
                else if (drawPathways)
                {
                    UnityEngine.Debug.DrawLine(new Vector3(i * width + offset, -j * width + offset, 0), new Vector3(i * width + offset, -(j+1) * width + offset, 0), Color.green, 60000);
                }
                if ((c.walls & 0x0001) == 0x0001)
                {
                    GameObject.Instantiate(wallPrefab, new Vector3(i * width, -j * width - offset, 0), Quaternion.Euler(0, 0, 90), wallContainer.transform).name = "wall [" + i + "," + j + "] " + c.walls.ToString("X4") + " [E]";
                }
                else if (drawPathways)
                {
                    UnityEngine.Debug.DrawLine(new Vector3(i * width - offset, -j * width - offset, 0), new Vector3((i+1) * width - offset, -j * width - offset, 0), Color.green, 60000);
                }
                if (i == _maze.GetLength(0) - 1)
                {
                    GameObject.Instantiate(wallPrefab, new Vector3((i + 1) * width, -j * width - offset, 0), Quaternion.Euler(0, 0, 90), wallContainer.transform).name = "wall [" + i + "," + j + "] " + c.walls.ToString("X4");
                }
                if (j == _maze.GetLength(1) - 1)
                {
                    GameObject.Instantiate(wallPrefab, new Vector3(i * width + offset, -(j + 1) * width, 0), Quaternion.Euler(0, 0, 0), wallContainer.transform).name = "wall [" + i + "," + j + "] " + c.walls.ToString("X4");
                }
            }
        }
    }

    private Cell getNotVisitedCell(int x, int y, ref Cell currCell)
    {
        currCell.visited = true;

        List<Direction> neighbours = new List<Direction>();
        if (y - 1 > -1 && !isVisited(_maze[x, y-1]))
            neighbours.Add(Direction.N);
        if (x - 1 > -1 && !isVisited(_maze[x-1, y]))
            neighbours.Add(Direction.W);

        if (y + 1 < _maze.GetLength(0) && !isVisited(_maze[x, y+1]))
            neighbours.Add(Direction.S);
        if (x + 1 < _maze.GetLength(1) && !isVisited(_maze[x+1, y]))
            neighbours.Add(Direction.E);

        if (neighbours.Count == 0)
            return null;

        Direction dir = neighbours[_rand.Next(neighbours.Count)];

        Cell nextCell = new Cell();
        // 0xNESW
        switch (dir)
        {
            default:
            case Direction.N:
                currCell.walls ^= 0x1000;
                y--;
                nextCell.walls ^= 0x0010;
                break;
            case Direction.E:
                currCell.walls ^= 0x0100;
                x++;
                nextCell.walls ^= 0x0001;
                break;
            case Direction.S:
                currCell.walls ^= 0x0010;
                y++;
                nextCell.walls ^= 0x1000;
                break;
            case Direction.W:
                currCell.walls ^= 0x0001;
                x--;
                nextCell.walls ^= 0x0100;
                break;
        }

        _maze[x, y] = nextCell;
        nextCell.setPos(x, y);

        return nextCell;
    }

    private bool isVisited(Cell cell)
    {
        if (cell != null)
            return cell.visited;
        else
            return false;
    }
}

public class Cell
{
    public Cell() { }
    public Cell(int x, int y) { setPos(x, y); }
    public void setPos(int x, int y) { _x = x; _y = y; }
    public int getX() { return _x; }
    public int getY() { return _y; }

    int _x, _y;
    public bool visited = false; 
    public int walls = 0x1111; // 0xNESW
}