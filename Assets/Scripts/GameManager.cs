﻿using UnityEngine;
using System.Collections;
using System.Diagnostics;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

    public GameObject wallPrefab;
    public GameObject wallContainer;
    public GameObject ground;

	[HideInInspector]
	public bool generatedLevel = false;

	void Start () {
        generateMazeLevel();
	}

    public void generateMazeLevel()
    {
        Stopwatch stopwatch = Stopwatch.StartNew();
        stopwatch.Start();
        MazeManager mazeGen = new MazeManager(20, 20);
        mazeGen.generateMaze();
        mazeGen.drawMaze(wallPrefab, wallContainer, false);
        UnityEngine.Debug.Log("Maze Level generation time = " + stopwatch.Elapsed);
    }
}
